<?php

/**
 * @file
 */
 
class views_node_taxonomy_filter_handler_filter_tid extends views_handler_filter {

  /**
   * Determine if a filter can be exposed.
   */
  function can_expose() { return FALSE; }

  function option_definition() {
    $options = parent::option_definition();
    $options['operator'] = array('default' => 'is');

    return $options;
  }
  
  function operator_options() {
    if ($this->options['multiple']) {
      return array(
        'is' => t('Is one of'),
        'is not' => t('Is not one of'),
      );
    }
    else {
      return array(
        'is' => t('Is'),
        'is not' => t('Is not'),
      );
    }
  }
    
  /**
   * Supply an edit form for the Filter.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $vocabs = array();
    $result = db_query("SELECT v.vid, v.name FROM {vocabulary} v ");
    while($vocab = db_fetch_object($result)) {
      $vocabs[$vocab->vid] = $vocab->name;
    }
      
    $default_value = $this->options['value']; 
    
    $form['value'] = array(
      '#type' => 'select',
      '#title' => t('Vocabulary'),
      '#options' => $vocabs,
      '#default_value' => $default_value,
      '#description' => t('Select the Vocabulary.'),
      '#multiple' => TRUE,
    );
  }
  
  /**
   * Display the value of the Filter selection in the Views edit screens.
   */
  function admin_summary() {
    $vocabularies =  taxonomy_get_vocabularies(); 
    if (is_array($this->value)) {
      $vocab_name = '';
      foreach ($this->value as $v) {
        $vocab = $vocabularies[$v];        
        $vocab_name .= $vocab->name . ', ';
      }
    }
    else {
      $vocab = $vocabularies[$this->value];
      $vocab_name = $vocab->name;
    }
    return t(" for Vocabulary: ") . $vocab_name;
  }

  /**
   * Modify the query appropriately.
   */
  function query() {
  
    if (empty($this->value)) return;
    $allowed_vocabs = $this->value;
    
    if (arg(0) != 'node') {
      return;
    }
    
    $nid = arg(1);
    
    if (!is_numeric($nid)) {
      return;
    }
    
    $node = node_load($nid);
    $taxonomy = $node->taxonomy; 
    
    $terms = array();
    if (sizeof($taxonomy)<1) return;
    foreach ($taxonomy as $term) {
      if ( in_array($term->vid,$allowed_vocabs) && $term->tid && is_numeric($term->tid)) { //it's an allowed term
        $terms[] = $term->tid;
      }
    }
    
    if (sizeof($terms) < 1) return;
    $tids = implode(',', $terms);

    //$country_tids = '307,110';
  
    $nid = $this->value;  
    $alias = $this->query->ensure_table('term_node');
    $this->query->add_where($this->options['group'], "{$alias}.tid in (%s)", $tids);      
  }
}